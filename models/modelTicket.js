var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ticketSchema = new Schema({

	issueType: String,
	issueOther: String,
	issueReporterType: String,
	issueReporterContactName: String,
	issueReporterContactEmail: String,
	issueLocation: String,
	issueSID: String,
	issueRequiredBy: Number,
	issueAssignedTo: String,
	issueImportance: String,
	issueStatus: String,

	issueUpdates: [{

		support: {
			type: Schema.ObjectId,
			ref: 'User'
		},
		issueDetail: String,
		issueUpdatedAt: {
			type: Number,
			default: Date.getTime
		},
		issueImportance: String,
		issueStatus: String
	  
	}],

});

var Ticket = mongoose.model('Ticket', ticketSchema);

module.exports = Ticket;
