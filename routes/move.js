'use strict';
var express = require('express');
var router = express.Router();
var mongoose = require('../app').mongoose;

var Move = require('../models/modelMove');
var User = require('../models/modelUser');
var Message = require('../models/modelMessage');

router.get('/', isLoggedIn, function (req, res) {

	Message.find({support: req.user.id, messageRead: false})
	.exec(function(err, messages) {
		if (err) throw err;

		User.find({}, function(err, supports) {
			if (err) throw err;		
			res.render('move.html', { user: req.user, messages: messages, supports: supports });
		});
	});
	
});

router.get('/:id', function (req, res) {
	
	Move.findOne({_id: req.params.id})
	.populate('moveUpdates.support')
	.exec(function(err, move) {
		if (err) throw err;
		res.render('moveShow.html', {user: req.user, move: move});
	});
	
});
	

router.post('/', isLoggedIn, function (req, res) {
	
	req.body.moveStatus = "Open";
	
	var move = new Move(req.body);
	move.moveRequiredBy = Date.parse(req.body.moveRequiredBy);
	move.moveUpdates.push(req.body);
	move.moveUpdates[0].support = req.user;
	move.moveUpdates[0].moveUpdatedAt = new Date().getTime();
	
	move.save(function(err) {
		if (err) throw err;
		
		res.redirect('/');
	});
	
});

router.post('/update/:id', isLoggedIn, function (req, res) {
	
	Move.findOne({_id: req.params.id}, function(err, move) {
		if (err) throw err;
		
		move.moveUpdates.push(req.body);
		move.moveUpdates[move.moveUpdates.length - 1].support = req.user;
		move.moveUpdates[move.moveUpdates.length - 1].moveUpdatedAt = new Date().getTime();
		move.moveStatus = req.body.moveStatus;
		
		move.save(function (err) {
			if (err) throw err;
			console.log('Move updated');
		});
		
		res.redirect('/');
	});
});



/*

  Helpers for checking state and such

*/

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect('/login');
}

module.exports = router;
