'use strict';
var express = require('express');
var router = express.Router();
var mongoose = require('../app').mongoose;

var User = require('../models/modelUser');
var Ticket = require('../models/modelTicket');
var Message = require('../models/modelMessage');

router.get('/', isLoggedIn, function (req, res) {

	Message.find({support: req.user.id, messageRead: false})
	.exec(function(err, messages) {
		if (err) throw err;
		
		User.find({}, function(err, supports) {
			if (err) throw err;
			res.render('issue.html', { user: req.user, supports: supports, messages: messages });
		});
	});

});

router.get('/:id', function (req, res) {
	
	Ticket.findOne({_id: req.params.id})
	.populate('issueUpdates.support')
	.exec(function(err, ticket) {
		if (err) throw err;
		res.render('issueShow.html', {user: req.user, ticket: ticket});
	});
	
});

router.post('/', isLoggedIn, function (req, res) {

    req.body.issueStatus = 'Open';
	
	var ticket = new Ticket(req.body);
	ticket.issueRequiredBy = Date.parse(req.body.issueRequiredBy);
	ticket.issueUpdates.push(req.body);
	ticket.issueUpdates[0].support = req.user;
	ticket.issueUpdates[0].issueUpdatedAt = new Date().getTime();
	
	ticket.save(function (err) {
		if (err) throw err;
		console.log('Ticket created');
	});
    
    res.redirect('/');
	
});

router.post('/update/:id', isLoggedIn, function (req, res) {
	
	req.body.issueUpdatedAt = Date();
	
	Ticket.findOne({_id: req.params.id}, function(err, ticket) {
		if (err) throw err;

		ticket.issueUpdates.push(req.body);
		ticket.issueUpdates[ticket.issueUpdates.length - 1].support = req.user;
		ticket.issueUpdates[ticket.issueUpdates.length - 1].issueUpdatedAt = new Date().getTime();
		
		ticket.issueImportance = req.body.issueImportance;
		ticket.issueStatus = req.body.issueStatus;
		
		if (req.body.issueStatus == 'Closed') {
			ticket.issueClosedOn = Date();
		}
		
		ticket.save(function (err) {
			if (err) throw err;
			console.log('Ticket updated');
		});
		
		res.redirect('/');
	});
});

/*

  Helpers for checking state and such

*/

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect('/login');
}

module.exports = router;



