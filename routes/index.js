﻿'use strict';
var express = require('express');
var router = express.Router();
var mongoose = require('../app').mongoose;
var mg = require('mongoose');

var Ticket = require('../models/modelTicket');
var Move = require('../models/modelMove');
var Message = require('../models/modelMessage');

router.get('/', function (req, res) {


	Ticket.find({issueStatus: 'Open'})
	.populate('issueUpdates.support')
	.exec(function(err, issues) {
		if (err) throw err;
		
		Ticket.find({issueStatus: 'Closed'})
		.sort({issueClosedOn: -1})
		.limit(5)
		.populate('issueUpdates.support')
		.exec(function(err, issuesClosed) {
			if (err) throw err;
			
			Move.find({moveStatus: 'Open'})
			.sort({moveDate: -1})
			.exec(function (err, moves) {
				if (err) throw err;
				
				Move.find({moveStatus: 'Closed'})
				.sort({moveDate: -1})
				.limit(5)
				.exec(function (err, recentMoves) {
					if (err) throw err;
					
					if (req.user) {
						var ObjectId = req.user.id;
					} else {
						var ObjectId = new mg.Types.ObjectId;
					}
					
					Message.find({support: ObjectId, messageRead: false})
					.exec(function(err, messages) {
						if (err) throw err;
						
						res.render('index.html', {
							tickets: issues, 
							issuesClosed: issuesClosed, 
							moves: moves,
							recentMoves: recentMoves,
							messages: messages,
							user: req.user
						});
					});
				});
			});
			
		});
	});
});

module.exports = router;
