var express = require('express');
var router = express.Router();

var mongoose = require('../app').mongoose;
var passport = require('../app').passport;

var User = require('../models/modelUser');
var Message = require('../models/modelMessage');

router.get('/', isLoggedIn, function (req, res) {

	Message.find({support: req.user.id, messageRead: false})
	.exec(function(err, messages) {
		if (err) throw err;
		User.find({}, function(err, supports) {
			res.render('config.html', {user: req.user, supports: supports, messages: messages});
		});
	});
});

router.post('/add', isLoggedIn, function (req, res) {
	
	console.log("Support added.");
	console.log(req.body);
		
//	var support = new Support(req.body);
//	support.save(function(err) {
//		if (err) throw err;
//		res.sendStatus(200);
//	});

	passport.authenticate('local-signup', {
	  successRedirect: '/config',
	  failureRedirect: '/config',
	  failureFlash: false
	})(req, res);
	
	
});

router.get('/delete/:id', isLoggedIn, function (req, res) {
	
	console.log("Support deleted.");
	
	User.remove({_id: req.params.id}, function (err) {
		if (err) throw err;
		res.sendStatus(200);
	});
	
});

router.get('/:id', isLoggedIn, function (req, res) {

	User.findOne({_id: req.params.id}, function(err, support) {
		res.setHeader('Content-Type', 'application/json');
		res.send(support);
	});
	
});

/*

  Helpers for checking state and such

*/

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect('/login');
}


module.exports = router;

