var express = require('express');
var router = express.Router();

var mongoose = require('../app').mongoose;
var passport = require('../app').passport;
var User = require('../models/modelUser');

//  errors:
//    loginErrorUsername: Username is incorrect
//    LoginErrorPassword: Password is incorrect


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('login.html', { message: req.flash() });
});

router.post('/', passport.authenticate('local-login', {
	successRedirect : '/',
	failureRedirect : '/login',
	failureFlash : true
}));

/*

  Helpers for checking state and such

*/

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect('/login');
}

module.exports = router;

